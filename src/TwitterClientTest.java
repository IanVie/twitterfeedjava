/************************************************************************* 
 *  Compilation:  javac TwitterClientTest.java
 *  Execution:    java TwitterClientTest 
 *  Dependencies: TwitterClient.java
 *     
 *  @author Ian Vurayai, ianvurayai@gmail.com
 *  
 *************************************************************************/
import static org.junit.Assert.*;

import org.junit.Test;

public class TwitterClientTest {

	/**
	 * Testing if the constructor works as intended 
	 */
	@Test
	public void twitterClientNormalWorkFlow() throws Exception  {
		TwitterClient ff = new TwitterClient("user.txt", "tweet.txt");
		ff.display();
	}

	/**
	 * Testing if error handling, if the user file doesn't have a 'Follow' keyWord
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testUserFileWithoutFollowKeyWord() throws Exception {
		new TwitterClient("user1.txt", "tweet.txt");

	}

	/**
	 * Testing if user has followers from file else through error
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testIfClientHasFollowers() throws Exception {
		new TwitterClient("user2.txt", "tweet.txt");
	}

	/**
	 * Testing if user with no followers, empty followers
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void errorUserWithNoFollowers() throws Exception {
		new TwitterClient("user3.txt", "tweet.txt");
	}

	/**
	 * Assumption: last on followers list must not have a comma at the end
	 * Checking if commas balance. 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void checkingCommas() throws Exception {
		new TwitterClient("user4.txt", "tweet.txt");
	}

	/**
	 * Assumption: Users name are unique
	 * Checking if user 'x' is trying to follow user 'x'
	 */
	@Test(expected = IllegalArgumentException.class)
	public void youCannotFolowYourself() throws Exception{
		new TwitterClient("user5.txt", "tweet.txt");
	}

	/**
	 * Assumption: users have a name longer than two letters and starts with caps
	 * Test if user as illegal name
	 */
	@Test(expected = IllegalArgumentException.class)
	public void illegalName() throws Exception {
		new TwitterClient("user6.txt", "tweet.txt");
	}

	/**
	 * The last user does not have a comma
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testingAgainsCommas() throws Exception {
		new TwitterClient("user8.txt", "tweet.txt");
	}

	/**
	 * Assumption: in a text only on '>' is allowed, its a special character
	 * Testing error handling if there is more than one '>'
	 */
	@Test(expected = IllegalArgumentException.class)
	public void moreThanOneGreaterSighn() throws Exception {
		new TwitterClient("user.txt", "tweet1.txt");
	}

	/**
	 * Test if the tweet line does not have a greater sign and through an exception 
	 * @throws Exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void noGreaterSighn() throws Exception {
		new TwitterClient("user.txt", "tweet2.txt");

	}
	/**
	 * Check if tweet exceeds the limit 140
	 */
	@Test(expected = IllegalArgumentException.class)
	public void tweetToLong() throws Exception {
		new TwitterClient("user.txt", "tweet3.txt");

	}
	/**
	 * Assumptions: All names in tweets file exist in users file
	 * If the user in tweet but not in users then throw an exception 
	 * @throws Exception
	 */
	@Test(expected = Exception.class)
	public void nameInTweetsNotInUserName() throws Exception {
		new TwitterClient("user.txt", "tweet4.txt");

	}
}
