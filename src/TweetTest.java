/*************************************************************************
 *  Compilation:  javac TweetTest.java
 *  Execution:    java TweetTest 
 *
 *  Tests Tweet object
 *  @author Ian Vurayai ianvurayai@gmail.com
 *
 *************************************************************************/
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

public class TweetTest {

	/**
	 * Through an error if time is negative
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testingNegativeTime() {
		new Tweet(Integer.MIN_VALUE, "Normal Tweet");
	}
	/**
	 * Testing if the tweet exceed 140 characters 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testingLongTweet() {
		new Tweet(1,"The tweet exceets the limit The tweet exceets the limit "
				+ "The tweet exceets the limit The tweet exceets the limit "
				+ "The tweet exceets the limit The tweet exceets the limit "
				+ "The tweet exceets the limit The tweet exceets the limit ");
	}
	/**
	 * Testing if the constructor throws an error given an empty tweet
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testingEmptyTweet() {
		new Tweet(4, "");
	}

	/**
	 * Testing normal work flow of the constructor 
	 */
	@Test
	public void testingNomalFlow() {
		new Tweet(Integer.MAX_VALUE, "This should pass");
	}
	/**
	 * Testing if sorting tweets according to time, the oder they come in.
	 */
	@Test
	public void testingObjectSorting() {
		ArrayList<Tweet> tweets = new ArrayList<Tweet>(); 
		tweets.add(new Tweet(3, "Third position"));
		tweets.add(new Tweet(4, "Forth position"));
		tweets.add(new Tweet(1, "First position"));
		tweets.add(new Tweet(2, "Second position"));
		Collections.sort(tweets);
		assert(tweets.get(1).compareTo(new Tweet(2, "Second position")) == 0);
		assert(tweets.get(1).compareTo(new Tweet(1, "First position")) == 1);
		assert(tweets.get(1).compareTo(new Tweet(3, "Third position")) == -1);
		assert(tweets.get(3).toString().equals("Forth position"));

	}

}
