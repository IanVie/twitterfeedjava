/*************************************************************************
 *  Compilation:  javac TwitterModelTest.java
 *  Execution:    java TwitterModelTest 
 *
 *  Tests Tweet object
 *  @author Ian Vurayai ianvurayai@gmail.com
 *
 *************************************************************************/

import static org.junit.Assert.*;

import org.junit.Test;

public class TwitterModelTest {
	/**
	 * Testing all the requirements are met code flow
	 * @throws Exception 
	 */
	@Test
	public void normalCodeFlow() throws Exception {
			TwitterModel t = new TwitterModel("Ian");
			t.addFollowers("Vie");
			assert(t.getFollowers().size() == 1);
			t.addTweet("Not so long tweet");
			t.addTime(1);
			assert(t.getTimeStaps().size() == 1);
			assert(t.getTweets().size() == 1);
			assert((t.getName().equals("Ian")));
			t.setName("Vavava");
			t.toString();

	}
	/**
	 * Testing if the code can handle empty twitter
	 * @throws Exception 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void addingEmptyTweet() throws Exception {
			TwitterModel t = new TwitterModel("Ian");
			t.addFollowers("Vie");
			assert(t.getFollowers().size() == 1);
			t.addTweet("Not so long tweet");
			assert(t.getTweets().size() == 1);
			t.addTweet("");

	}

	/**
	 * Testing handling the update name to same as 
	 * the one of the followers
	 * @throws Exception 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void settingYourNameToFollowersName() throws Exception {
			TwitterModel t = new TwitterModel("Ian");
			t.addFollowers("Vie");
			t.setName("Vie");
	}
	/**
	 * Testing tweet length, the tweet must be at most 140
	 * @throws Exception 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void twitterModelTweetWithMoreThan140() throws Exception {
			TwitterModel t = new TwitterModel("Ian");
			t.addFollowers("Vie");
			assert(t.getFollowers().size() == 1);
			t.addTweet("a01234567890123456789012345678901234567890123456"
					+ "7890123456789012345678901234567890123456789012345"
					+ "67890123456789012345678901234567890123456789");
	
	}

	/**
	 * Testing user updating its name
	 * @throws Exception 
	 */
	public void twitterModelUpdateNameWithLegalName() throws Exception {
			TwitterModel t = new TwitterModel("Ian");
			t.setName("Ianvie");
	}

	/**
	 * Testing if the user tries to update name to the one they already using
	 * @throws Exception 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void twitterModelUpdateNameWithSameName() throws Exception {
			TwitterModel t = new TwitterModel("Ian");
			t.setName("Ian");
	}

	/**
	 * Testing if followers name met the names requirements 
	 * @throws Exception 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void twitterModelAddFollowerWithIllegalName() throws Exception {
			TwitterModel t = new TwitterModel("Ian");
			t.addFollowers("ian");

	}
}
