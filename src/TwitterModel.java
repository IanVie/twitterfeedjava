/*************************************************************************
 *  Compilation:  javac TwitterModel.java
 *  Execution:    java TwitterModel
 *  Dependencies: StdIn.java
 *  Data files:   https://bitbucket.org/IanVie/twitterfeedjava/src/master/
 *  
 *  @author Ian Vurayai, ianvurayai@gmail.com
 *  
 *************************************************************************/
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import javax.naming.NamingException;

public class TwitterModel {

	private String name;
	private ArrayList <String> followers = new ArrayList<String>();
	private ArrayList <String> tweets =  new ArrayList<String>();
	private ArrayList <Integer> timeStamp =new ArrayList <Integer>();
	
	/**
	 *TwitterModel constructor without parameters
	 * Its private constructor enforce that TwitterModel must have parameters
	 */
	@SuppressWarnings("unused")
	private TwitterModel() {
		this.name = null;
		this.followers = null;
		this.tweets = null;
		this.timeStamp = null;
	}
	/**
	 * Creates a TwitterModel with user names as a parameter
	 * @param name, user's username
	 * @throws Exception if the name is empty then 
	 */
	public TwitterModel(String name) throws Exception {
		if (!name.matches("[A-Z][a-zA-Z]+")) {
			throw new IllegalArgumentException("Name must start with a Caps and have length more than one");
		} else {
			this.name = name;
		}
	}
	/**
	 * Out of scope 
	 * @param name
	 * @param followers
	 * @param tweets
	 * @param timeStamp
	 * @throws Exception
	 */
	private TwitterModel(String name, ArrayList<String> followers, ArrayList<String> tweets, ArrayList<Integer> timeStamp) throws Exception {
		if (!name.matches( "[A-Z][a-zA-Z]+")) {
			throw new IllegalArgumentException("Name must start with a Caps and have length more than one"); 
		} 
		else if (followers.isEmpty()) {
			throw new IllegalArgumentException("Followers can't be empty");
		}
		else if (tweets.isEmpty()) {
			throw new IllegalArgumentException("Tweets can't be empty");
		}else {
			this.name = name;
			this.followers = followers;
			this.tweets = tweets;
			this.timeStamp =timeStamp;
		}
	}
	/**
	 * The name must be two letters and long, the name starts with cap
	 * Methods for users to update there user name once they register
	 * @param name
	 */

	public void setName(String name) {
		if (!name.matches("[A-Z][a-zA-Z]+")) {
			throw new IllegalArgumentException("Name must start with a Caps and have length more than one");
		} else if(this.name.equals(name)) {
			throw new IllegalArgumentException("The name is the same as the old name");
		} if(this.followers.contains(name)) {
			throw new IllegalArgumentException("Name already exists");
		} else {
			this.name = name;
		}
	}
	/**
	 * Functionality that allows addition of followers to a user
	 * @param followers
	 * @throws Exception
	 */
	private void setFollowers(ArrayList<String> followers) throws Exception {
		if (!followers.isEmpty()) {
			this.followers = followers;
		} else {
			throw new IllegalArgumentException("Tweets are empty");
		}
	}
	/**
	 * Add twitter to a current 
	 * @param tweets
	 * @throws Exception
	 */
	private  void setTweets(ArrayList<String> tweets) throws Exception {
		if (!tweets.isEmpty()) {
			this.tweets = tweets;	
		} else{
			throw new IllegalArgumentException("Tweets are empty");
		}

	}
	/**
	 * Names are unique 'x' cannot follow 'x'
	 * Add one follower to user's followers
	 * @param follower
	 * @throws Exception
	 */
	public void addFollowers(String follower) throws Exception {
		if (!follower.matches("[A-Z][a-zA-Z]+")) {
			throw new IllegalArgumentException("Follower can't be empty"); 
		} else if (follower.equals(this.name)) {
			throw new Exception("You cannot follow youself");
		}
		else if (!this.followers.contains(follower)) {
			this.followers.add(follower);
		}
	}
	/**
	 * Adds one tweet to users tweets
	 * @param tweet
	 * @throws Exception
	 */
	public void addTweet(String tweet) throws Exception {
		if (tweet.length() > 140) {
			throw new IllegalArgumentException("Tweet exceeded the max length 140"); 
		} else if(tweet.isEmpty()) {
			throw new IllegalArgumentException("Tweet can't be empty"); 
		} else {
			this.tweets.add(tweet);
		}
	}
	/**
	 * Add the position of tweet on timeline
	 * @param time
	 * @throws Exception
	 */
	public void addTime(int time) throws Exception {
		if(time<0) {
			throw new IllegalArgumentException("Cannot have negative time "); 	
		} else {
			this.timeStamp.add(time);
		}
	}
	/**
	 * @return the name of twitter user account holder
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the timetStamp of a specific twitter
	 */
	public ArrayList<Integer> getTimeStaps() {
		return timeStamp;
	}
	/**
	 * @return the list of all followers of the user
	 */

	public ArrayList<String> getFollowers() {
		return followers;
	}
	/**
	 * @return the currents users tweets
	 */
	public ArrayList<String> getTweets() {
		return tweets;	
	}

	/**
	 * for debugging purpose
	 */
	public String toString() {
		return "Name : " + name +"\n" +
				"Followers: " + followers +"\n" +
				"Tweets : " + tweets +"\n" +
				"TimeStamps : " + timeStamp;
	}
}
