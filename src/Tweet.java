/*************************************************************************
 *  Compilation:  javac Tweet.java
 *  Execution:    java Tweet   (basic test)
 *
 *  Tweet object with time and tweet text as its properties 
 *  @author Ian Vurayai ianvurayai@gmail.com
 *
 *************************************************************************/

public class Tweet  implements Comparable<Tweet> {

	private final int time;
	private final String tweetText;

	@SuppressWarnings("unused")
	private Tweet() {
		this.time = 0;
		this.tweetText = "Tweet place holder";
	}
	public Tweet(int time, String tweet) {
		if (tweet.length() > 140) {
			throw new IllegalArgumentException("Tweet exceeded the max length 140"); 
		} if (time < 0) {
			throw new IllegalArgumentException("Cannot have negative time "); 	 
		} if (tweet.isEmpty()) {
			throw new IllegalArgumentException("Tweet can't be empty"); 
		} else {
			this.time = time;
			this.tweetText = tweet;
		}
	}
	/**
	 * Assumption the time of tweets its unique
	 * sort tweets according to time
	 */
	@Override
	public int compareTo(Tweet tweet) {
		return time == tweet.time ? 0 : time > tweet.time? 1 : -1;
	}
	/**
	 * Returns the tweet text
	 */
	public String toString() {
		return tweetText;
	}
}
