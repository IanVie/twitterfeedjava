/*************************************************************************
 *  Given the users, followers and tweets, the objective is to display a 
 *  simulated twitter feed for each user to the console.
 *  The program will receive two seven-bit ASCII files. The first file contains 
 *  a list of users and their followers. The second file contains tweets. 
 *  Given the users, followers and tweets, the objective is to display a 
 *  simulated twitter feed for each user to the console.
 * 
 *  Assumptions: 
 *   Than name start with a caps and has a length of more than one.
 *   Every line in users has a 'follow' keyword.
 *   Every user must have a relationship with other user.
 *   Last user followers has no ',' comma
 *   User names are unique
 *   Users cannot follow themselves
 *   In a text only on '>' is allowed, its a special character
 *   Names that exist in 'tweet' also exist in 'user'
 *  
 *  
 *  Compilation:  javac TwitterClient.java
 *  Execution:    java TwitterClient user.txt tweet.txt
 *  Dependencies: StdIn.java, Tweet.java, TwitterModel.java
 *  Data files:   https://bitbucket.org/IanVie/twitterfeedjava/src/master/
 *  
 *  @author Ian Vurayai, ianvurayai@gmail.com
 *  
 *************************************************************************/

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
public class TwitterClient {
	public ArrayList <TwitterModel> accounts =  new ArrayList<TwitterModel>();
	public ArrayList <String> allUsers =  new ArrayList<String>();
	public ArrayList <Tweet> tweets =  new ArrayList<Tweet>();
	public In in;	
	public String usersFiles;
	public String tweetsFiles;

	/**
	 * Twitter client constructor 
	 * @param usersFiles, path to the source file
	 * @param tweetsFiles, path to the source file
	 */
	public TwitterClient(String usersFiles, String tweetsFiles) throws Exception {
		this.usersFiles = usersFiles;
		this.tweetsFiles = tweetsFiles;
		readUsersFromFile();
		readTweetsFromFile();
		display();
	}

	/**
	 * Assumption: one users followers are in one line in text file
	 * Read user files
	 * @throws Exception
	 * Each line of a well-formed user file contains a user, followed by the word 'follows' and then a comma separated list of users 
	 * they follow. Where there is more than one entry for a user, consider the union of all these entries to determine the users 
	 * they follow.
	 */
	private void readUsersFromFile() throws Exception {
		in = new In(new File(this.usersFiles));
		String [] relations;
		TwitterModel temp = null;
		boolean duplicated;
		while (in.hasNextLine()) {
			relations = in.readLine().split(" ");
			duplicated = false;
			if (relations.length <= 1) {
				throw new IllegalArgumentException("Error while reading file");
			} else if (!relations[1].equals("follows")) {
				throw new IllegalArgumentException("No keyword follows in users");
			} else if (relations.length <= 2) {
				throw new IllegalArgumentException("User doesn't have relationship");
			}  
			for (TwitterModel account : accounts) {
				if (account.getName().equals(relations[0])) {
					temp = account;
					duplicated = true;
				} 
			}
			if (!duplicated) {
				temp = new TwitterModel(relations[0]);
				if (!allUsers.contains(relations[0])) {
					allUsers.add(relations[0]);
				}
			}
			for (int i=1; i<relations.length; i++) {
				if (relations[0].equals(relations[i])) {
					throw new IllegalArgumentException("You cannot follow youself");
				} else if (relations.length > 3 && i >= 2 && i<relations.length-1 && !(relations[i].charAt(relations[i].length()-1) == ',')) {
					throw new IllegalArgumentException("Followers must be comma seperated");
				} else if (i!=1 && relations[i].charAt(relations[i].length()-1) == ',') {
					if (i == relations.length-1) {
						throw new IllegalArgumentException("Last Follower must not have comma seperated");
					}
					temp.addFollowers(relations[i].substring(0, relations[i].length()-1));
					if (!allUsers.contains(relations[i].substring(0, relations[i].length()-1))) {
						allUsers.add(relations[i].substring(0, relations[i].length()-1));
					}
					if (!duplicated) {
						accounts.add(temp);
					}
				} else if(i != 1) {
					temp.addFollowers(relations[i]);
					if (!allUsers.contains(relations[i])) {
						allUsers.add(relations[i]);
					}
					if (!duplicated) {
						accounts.add(temp);
					}
				}
			}
		}
	}

	/**
	 * Assumption '>' its a special sighn it cannot be used anywhereelse
	 * Reads tweets from file
	 * @throws Exception
	 * Assumption Only one sign is alloved in line
	 * Lines of the tweet file contain a user, followed by greater than, space and then a tweet
	 * that may be at most 140 characters in length. The tweets are considered to be posted by
	 * the each user in the order they are found in this file.
	 */
	private void readTweetsFromFile() throws Exception {
		in = new In(new File(this.tweetsFiles));
		String nameTweet;
		int time=0;
		boolean nameNotFound = true;
		while (in.hasNextLine()) {
			nameTweet = in.readLine();
			time++;
			if (nameTweet.split(">").length>2) {
				throw new IllegalArgumentException("Tweet contain more than one '>' relationship sighn");
			} else if (!nameTweet.contains(">")) {
				throw new IllegalArgumentException("Tweet does not contain the '>' relationship sighn");
			} else if (!(nameTweet.split(">")[1].trim().length()<141)) {
				throw new IllegalArgumentException("Tweet max characters 140");
			} else {
				if (accounts.isEmpty()) {
					throw new IllegalArgumentException("No accounts");
				} else {
					for (TwitterModel account : accounts) {
						nameNotFound = true;
						if (account.getName().equals(nameTweet.split(">")[0])) {
							account.addTweet((nameTweet.split(">")[1]).trim());
							account.addTime(time);
							nameNotFound = false;
							break;
						}
					}
					if (nameNotFound) {
						throw new Exception("Name associated with this tweet does not exist");	
					}
				}
			}
		}
	}

	/**
	 * Writes the results to the terminal
	 */
	public void display(){	
		Collections.sort(allUsers);
		for (String user: allUsers) {
			System.out.println(user);
			tweets =  new ArrayList<Tweet>();
			for (TwitterModel account : accounts) {
				if (user.equals(account.getName())) {
					for (int k =0; k<account.getTweets().size(); k++) {
						tweets.add(new Tweet(account.getTimeStaps().get(k), "@" + user +
								": " + account.getTweets().get(k)));
					}
					for (String follower : account.getFollowers()) {
						for (TwitterModel subAccount : accounts) {
							if (subAccount.getName().equals(follower) && !subAccount.getTweets().isEmpty()) {
								for(int n=0;n<subAccount.getTweets().size();n++){
									tweets.add(new Tweet(subAccount.getTimeStaps().get(n), "@"+follower+": "
											+subAccount.getTweets().get(n)));
								}
							}

						}
					}					
					Collections.sort(tweets);
					for (Tweet tweet : tweets) {
						System.out.println(tweet);
					}
				}
			}
		}

	}

	public static void main(String[] args) throws Exception {
		if(args.length==2){
			new TwitterClient(args[0], args[1]);
		}else{
			System.out.println("java TwitterClient <path/user.txt> <path/tweet.txt>");
		}
		
	}

}
